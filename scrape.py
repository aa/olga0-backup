import requests
import html5lib
import urllib.request, urllib.parse, urllib.error
import urllib.parse
import os
import errno

from cssselect import GenericTranslator, SelectorError
from urllib.parse import urljoin

import lxml.etree as ET


def mkdir_p(path):
    try:
        os.makedirs(path)
    except OSError as exc: # Python >2.5
        if exc.errno == errno.EEXIST and os.path.isdir(path):
            pass
        else: raise


def download_asset(src):
    if not src.startswith("http"):
        print(src)
        src = urljoin("http://sarma.be", src)

    if src not in downloaded:
        print(("downloading " + src))
        request = requests.get(src)
        url_parsed = urllib.parse.urlparse(src)
        path = os.path.join(url_parsed.netloc, url_parsed.path[1:])
        mkdir_p(os.path.split(path)[0])
        f = open(path, "w")
        f.write(request.text)
        f.close()
        downloaded.append(src)
# 
# 
# def absolutize(tree):
#     expression = GenericTranslator().css_to_xpath('[href]')
#     for elt in tree.xpath(expression):
#         href = elt.attrib['href']
#         if not elt.attrib['href'].startswith('#'):
#             elt.attrib['href'] = urljoin(url, href)
# 
#     expression = GenericTranslator().css_to_xpath('[src]')
#     for elt in tree.xpath(expression):
#         src = elt.attrib['src']
#         elt.attrib['src'] = urljoin(url, src)



def main(url, downloaded):
    """
    Downloads the given URL.
    """
    ##
    # Downloads the page and parses it
    ##
    request = requests.get(url)
    output = request.text

    parser = html5lib.html5parser.HTMLParser(tree=html5lib.getTreeBuilder("lxml"), namespaceHTMLElements=False)
    tree = parser.parse(output)


    # ##
    # # Absolutize links (src and href)
    # ##
    # absolutize(tree)


    # ##
    # # Gets the embed url
    # ##
    expression = GenericTranslator().css_to_xpath('link[rel="aa-embed"]')
    # embed_base_url = tree.xpath(expression)[0].attrib['href']
    embed_base_url = "http://sarma.be/oralsite/embed/"


    # ##
    # # Downloads the embeds
    # ##
    expression = GenericTranslator().css_to_xpath('[rel="aa:embed"]')
    for elt in tree.xpath(expression):
        params = {
            'url': elt.attrib['href'].replace("oralsite.stdin.fr", "sarma.be"),
            'filter': elt.attrib.get('data-filter', '')
        }

        embed_url = embed_base_url + '?' + urllib.parse.urlencode(params)

        request = requests.get(embed_url)
        json = request.json()

        fragment = parser.parseFragment(json['content'])[0]
        elt.getparent().replace(elt, fragment)

        extra_css = []
        extra_js = []
        scripts = []

        # - extra_css: loads extra link rel="stylesheet"
        for src in json['extra_css']:
            extra_css.append(src)

        for src in set(extra_css):
            expression = GenericTranslator().css_to_xpath('head')
            parent = tree.xpath(expression)[0]

            expression = GenericTranslator().css_to_xpath('link[href="{}"]'.format(src))
            if not tree.xpath(expression):
                print("appending extra css -> " + src)

                elt = ET.Element("link") 
                elt.attrib['rel'] = "stylesheet"
                elt.attrib['type'] = "text/css"
                elt.attrib['href'] = src
                parent.append(elt)

        # - extra_js: load extra script
        for src in json['extra_js']:
            extra_js.append(src)

        for src in set(extra_js):
            expression = GenericTranslator().css_to_xpath('body')
            parent = tree.xpath(expression)[0]

            expression = GenericTranslator().css_to_xpath('script[src="{}"]'.format(src))
            if not tree.xpath(expression):
                print("appending extra js -> " + src)

                elt = ET.Element("script") 
                elt.attrib['src'] = src
                parent.append(elt)

        # - script: extra javascript code to execute
        if 'script' in json:
            scripts.append(json["script"])

        for script in scripts:
            expression = GenericTranslator().css_to_xpath('body')
            parent = tree.xpath(expression)
            elt = ET.Element("script") 
            elt.text = json["script"]
            parent.append(elt)


    ##
    # Downloads the javascripts
    # Download the stylesheets
    ##
    expression = GenericTranslator().css_to_xpath('script[src]')
    for elt in tree.xpath(expression):
        src = elt.attrib['src']
        download_asset(src)

    expression = GenericTranslator().css_to_xpath('link[rel="stylesheet"]')
    for elt in tree.xpath(expression):
        href = elt.attrib['href']
        download_asset(href)


    # ##
    # # Removes the superflous elements
    # ##

    # removes namespaces.css which is broken anyways
    # removes the browser panel
    # removes the browser panel
    expressions = [
        GenericTranslator().css_to_xpath('link[href$="namespaces.css/"]'),
        GenericTranslator().css_to_xpath('#browser-panel'),
        GenericTranslator().css_to_xpath('a.browselink')
    ]

    for expression in expressions:
        for elt in tree.xpath(expression):
            elt.getparent().remove(elt)


    ##
    # Serializes the tree and saves to file
    ##

    walker = html5lib.getTreeWalker("lxml")
    stream = walker(tree)

    serializer = html5lib.serializer.HTMLSerializer(quote_attr_values="always",
                    alphabetical_attributes=True,
                    omit_optional_tags=False)

    url_parsed = urllib.parse.urlparse(url)
    path = os.path.join(url_parsed.netloc, url_parsed.path[1:])
    path = urllib.parse.unquote(path) ## Added on the 11/12/2017
    mkdir_p(path)

    output = serializer.render(stream)
    output = output.replace("U0003A", ":") # fix unicode issue with xmlns
    f = open(os.path.join(path, "index.html"), "w")
    f.write(output)
    f.close()


if __name__ == '__main__':
    downloaded = []

    f = open("urls.lst", "r")
    for url in f.read().strip('\n').split("\n"):
        if url.startswith("#"):
            pass 
            # print(('skipping ' + url))
        else:
            print(("downloading " + url))
            main(url, downloaded)



